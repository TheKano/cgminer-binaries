This directory contains binary executables for the AntS1 mining device
and a replacement script for the cgminer-monitor script

There's a donation address at the bottom of this README
But rather than donate, why not simply mine at my pool instead :)
https://kano.is/

To update your executable:
--------------------------
Login to your ant, with ssh or putty, using the same root username and password
you use on the luci web interface
If your password is blank, then use the "System/Administration" tab in the web
interface to set a password, since you cannot login with a blank password

If you haven't done an update like this before (or undid the change), run the
following commands:
 cd /usr/bin/
 mv cgminer cgminer.original
 wget http://kano.is/AntS1/cgminer-ants1-4.9.1c-e6ad32a
 chmod +x cgminer-ants1-4.9.1c-e6ad32a
 ln -s cgminer-ants1-4.9.1c-e6ad32a cgminer

If you HAVE done an update like this before and cgminer.original has
already been saved, then run:
 cd /usr/bin/
 wget http://kano.is/AntS1/cgminer-ants1-4.9.1c-e6ad32a
 chmod +x cgminer-ants1-4.9.1c-e6ad32a
 rm cgminer
 ln -s cgminer-ants1-4.9.1c-e6ad32a cgminer

Next you need to restart cgminer on your AntS1
You can restart it, when logged into your ant, with the command
 /etc/init.d/cgminer restart
You will get a message saying "stopped cgminer" if it was running
You will get a message "no cgminer found; none killed" if it wasn't running
If you also get a message "cgminer is already running" then wait 10 seconds
then reissue the command to restart cgminer
 /etc/init.d/cgminer restart
The AntS1 system itself also checks to restart cgminer if it isn't running,
every 3 minutes

You can also restart it from the web interface by clicking the
"Save & Apply" button on the "Status/Miner Configuration" tab
It can sometimes take up to 3 minutes before the AntS1 restarts cgminer
if you use the web interface to restart it

Note that this binary file is also in my cgminer-binaries git here:
 https://github.com/kanoi/cgminer-binaries/tree/master/AntS1
However, wget on the AntS1 won't allow https by default so I've copied it
to my link specified in the wget above
The md5sum checksum is in:
 https://github.com/kanoi/cgminer-binaries/blob/master/MD5SUM
The md5sum value is:
 a341853fa6461efb054294336e9d78e9  AntS1/cgminer-ants1-4.9.1c-e6ad32a
You can check the md5sum value when logged in to the ant with:
 md5sum /usr/bin/cgminer-ants1-4.9.1c-e6ad32a

You can also request the version number of cgminer via the API
If you ssh into your ant, the command is
 cgminer-api version
For this version, it will reply with text containing:
 [CGMiner] => 4.9.1c
 [API] => 3.5

If you don't have one of the original AntS1 miners, but a newer one,
you may find that the luci "Miner Status" display shows LSTime on the end of
the pools table, as a long number rather than a date, after you update cgminer.
The easy fix for this - provided on the bitcointalk forum by artpego,
is to edit /usr/lib/lua/luci/controller/cgminer.lua to undo the change
commented out in there. Around line 287, look for:
  --lst_date = os.date("%c", lst)
  lst_date = lst
and change it back to:
  lst_date = os.date("%c", lst)
  --lst_date = lst

Note that you may need to power cycle your AntS1 for the above change to take
effect

The source for this binary cgminer version (4.9.1c-e6ad32a) is here:
 https://github.com/kanoi/cgminer/tree/ants1-4.9.1c-e6ad32a

To undo the change:
 cd /usr/bin/
 rm cgminer
 mv cgminer.original cgminer
then restart cgminer (as explained above)


To update the cgminer-monitor script:
-------------------------------------
This new cgminer-monitor script will kill and restart cgminer if the Elapsed
Time is greater than 4 weeks.
This is useful if the clock in the OpenWRT was wrong when cgminer started but
ntp has since corrected it - if this happened then the Elapsed Time will show,
incorrectly, a very large number and the calculated hash rates and other
numbers will be very small due to the large Elapsed Time value

Login to your ant, with ssh or putty, using the same root username and password
you use on the luci web interface
If your password is blank, then use the "System/Administration" tab in the web
interface to set a password, since you cannot login with a blank password

If you haven't done an update like this before (or undid the change), run the
following commands:
 cd /usr/bin/
 mv cgminer-monitor cgminer-monitor.original
 wget http://kano.is/AntS1/cgminer-monitor
 chmod +x cgminer-monitor
 
If you HAVE done an update like this before and cgminer-monitor.original has
already been saved, then run:
 cd /usr/bin/
 rm cgminer-monitor
 wget http://kano.is/AntS1/cgminer-monitor
 chmod +x cgminer-monitor

You won't need to power cycle your AntS1

To undo this change:
 cd /usr/bin/
 rm cgminer-monitor
 mv cgminer-monitor.original cgminer-monitor


Feel free to donate BTC to help support the developer :)
1Jjk2LmktEQKnv8r2cZ9MvLiZwZ9gxabKm
Or mine at my pool https://kano.is/
